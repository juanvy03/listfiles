
#!/usr/bin/env python3

#
# Solution 1:
#
import subprocess

directories = ['test/', 'live/']

for i in range (0, len(directories)):
    checkCommand = "find {} -type d".format(directories[i])
    executingCmd = subprocess.Popen(checkCommand, shell=True, stdout=subprocess.PIPE, executable='/bin/bash')
    dir_list = executingCmd.communicate()[0]
    dir_list = dir_list.splitlines()

    for x in range(0, len(dir_list)):

        list_command = 'find {} -type f |wc -l'.format(dir_list[x].decode('utf-8'))
        count = subprocess.Popen(list_command, shell=True, stdout=subprocess.PIPE, executable= "/bin/bash")
        list_files = count.communicate()[0]
        list_files = list_files.splitlines()
        print('{} has {} files'.format(dir_list[x].decode('utf-8'), list_files[0].decode('utf-8')))

#
# Solution 2 :
# Case only those folders exists in directory within script.
#
checkCommand = "find . -type d"
executingCmd = subprocess.Popen(checkCommand, shell=True, stdout=subprocess.PIPE, executable='/bin/bash')
dir_list = executingCmd.communicate()[0]
dir_list = dir_list.splitlines()

for x in range(0, len(dir_list)):

    list_command = 'find {} -type f |wc -l'.format(dir_list[x].decode('utf-8'))
    count = subprocess.Popen(list_command, shell=True, stdout=subprocess.PIPE, executable= "/bin/bash")
    list_files = count.communicate()[0]
    list_files = list_files.splitlines()
    print('{} has {} files'.format(dir_list[x].decode('utf-8'), list_files[0].decode('utf-8')))
